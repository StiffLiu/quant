from datetime import datetime
from collections import defaultdict
from matplotlib import pyplot

class FundPortfolio(object):

  def __init__(self, cache, portfolios):
    fmt = "%Y%m%d"
    convert = lambda p:(p[0], datetime.strptime(p[1], fmt).date(), p[2])
    portfolios = [convert(p) for p in portfolios]
    self.portfolios = sorted(portfolios, key=lambda p:p[1])
    self.cache = cache

  def one_return(self, symbol, investments):
    finfo, idx = self.cache.finfo_from_cache(symbol), 0
    acworth, dates = finfo.real_net_worth(), sorted(list(investments.keys()))
    while idx < len(acworth) and acworth[idx][0].date() < dates[0]:
      idx = idx + 1
    
    returns = []
    if idx < len(acworth):
      returns.append((acworth[idx][0], 0.0, 0.0))
      last_value, idx = investments[dates[0]], idx + 1
      def day_ret(i):
        new_worth, last_worth = acworth[i][1], acworth[i - 1][1]
        method, number = acworth[i][2], acworth[i][3]
        offset, ratio = 0.0, 1.0
        if method == 'd':
          offset = last_value * number / last_worth
        elif method == 's':
          ratio = number
        elif method is not None:
          raise ValueError(f"{method} can't be processed")
        ret = last_value * (new_worth - last_worth) / last_worth
        return ret + offset, last_value + ret, last_value
        
      for d in dates[1:]:
        while idx < len(acworth) and acworth[idx][0].date() < d:
          dr, last_value, worth = day_ret(idx)
          returns.append((acworth[idx][0], dr, worth))
          idx = idx + 1
        if idx >= len(acworth):
          break
        dr, last_value, worth = day_ret(idx)
        returns.append((acworth[idx][0], dr, worth))

        last_value, idx = last_value + investments[d], idx + 1
        if idx >= len(acworth):
          break

      while idx < len(acworth):
        dr, last_value, worth = day_ret(idx)
        returns.append((acworth[idx][0], dr, worth))
        idx = idx + 1
    return returns

  def returns(self):
    sym_to_invest = defaultdict(dict)
    sym_to_rtn = defaultdict(dict)
    for p in self.portfolios:
      invest = sym_to_invest[p[0]]
      invest[p[1]] = invest.get(p[1], 0.0) + p[2]
    for s, invest in sym_to_invest.items():
      sym_to_rtn[s] = self.one_return(s, invest)
    return sym_to_rtn

  def accum_returns(self, symbols=None):
    returns = self.returns()
    #for symbol, rtn in returns.items():
    #  print(symbol, " : ", rtn)
    day_ret = {}
    for s, r in returns.items():
      if (symbols is not None) and (s not in symbols):
        continue
      for d, v, w in r:
        ov, ow = day_ret.get(d, (0.0, 0.0))
        day_ret[d] = (ov + v, ow + w)
    acc_ret = [[k, day_ret[k][0], day_ret[k][1]] for k in sorted(day_ret.keys())]
    for i in range(1, len(acc_ret)):
      acc_ret[i][1] = acc_ret[i - 1][1] + acc_ret[i][1]
    return acc_ret

  def plot_accum_returns(self, symbols=None, plotter=None):
    plotter = plotter or pyplot
    returns = self.accum_returns(symbols)
    label = 'ALL' if symbols is None else '|'.join(symbols)
    plotter.plot([r[0] for r in returns], [r[1] for r in returns], label=label)
    plotter.legend()
    return returns
