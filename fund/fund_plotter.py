from matplotlib import pyplot
from scipy.stats import pearsonr
from fund_data_scraper import *

class FundPlotter(object):
    
  def __init__(self, base="/var/data/raw/fund.eastmoney.com/latest"):
    self.cache = create_cached_data(base)

  def plot_acworth(self, symbol, lookback_days, plotter=None, download_if_no_exist=False):
    finfo = self.cache.finfo_from_cache(symbol, download_if_no_exist=download_if_no_exist)
    if finfo.enabled:
      value = finfo.get_acworth(lookback_days)
      slc = lambda idx: [v[idx] for v in value]
      print(self.cache.funds[symbol])
      plotter = plotter or pyplot
      plotter.plot(slc(0), slc(1), label=symbol)

  def plot_acworths(self, symbols, lookback_days=None, plotter=None, download_if_no_exist=False):
    if not plotter:
      fig, plotter = pyplot.subplots()
    for s in symbols:
      self.plot_acworth(s, lookback_days, plotter, download_if_no_exist)
    plotter.legend()

  def plot_corr(self, s1, s2, lookback_days=None, is_day_return=False, plotter=None, download_if_no_exist=False):
    i1 = self.cache.finfo_from_cache(s1, download_if_no_exist=download_if_no_exist)
    i2 = self.cache.finfo_from_cache(s2, download_if_no_exist=download_if_no_exist)
    if i1.enabled and i2.enabled:
      d = align_data(i1.get_acworth(), i2.get_acworth(), lookback_days)
      slc = lambda idx: [v[idx] for v in d]
      slc1, slc2 = slc(1), slc(2)
      if is_day_return:
        slc1, slc2 = lag_diff(slc1), lag_diff(slc2)
      label = f"{s1}-{s2}"
      print(f"{label} correlation: {pearsonr(slc1, slc2)}")
      plotter = plotter or pyplot
      plotter.scatter(slc1, slc2, label=label)

  def plot_corrs(self, pairs, lookback_days=None, is_day_return=False, plotter=None):
    if not plotter:
      fig, plotter = pyplot.subplots()
    for p in pairs:
      self.plot_corr(p[0], p[1], lookback_days, is_day_return, plotter)
    plotter.legend()
