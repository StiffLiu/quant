from fund_portfolio import *
from fund_data_scraper import *
from datetime import datetime

class AddMoneyOnDrop(object):

  def __init__(self, symbols, drop=-0.03, base=None):
    self.cache, self.symbols, self.drop = CachedData(base), symbols, drop

  def calc_one(self, symbol, start_date=None):
    finfo = self.cache.finfo_from_cache(symbol)
    worth = finfo.real_net_worth()
    idx, cnt, fmt = 0, len(worth), "%Y%m%d"
    portfolios = []
    if start_date:
      sdate = datetime.strptime(start_date, fmt)
      while idx < cnt and worth[idx][0] < sdate:
        idx = idx + 1

    while idx + 1 < cnt:
      cur = worth[idx]
      if cur[4] < self.drop * 100:
        portfolios.append((symbol, datetime.strftime(worth[idx+1][0], fmt), 100))
      idx = idx + 1
    return portfolios

  def calc(self, start_date=None):
    portfolios = []
    for s in self.symbols:
      portfolios = portfolios + self.calc_one(s, start_date)
    return FundPortfolio(self.cache, portfolios)
