import click, json, os, requests, sqlite3, time, datetime, pickle, re, math
import statsmodels.api as sm
import dateparser
import dateutil.parser
from scipy.stats import pearsonr
from functools import lru_cache


def parse_date(datestr):
  dt = dateparser.parse(datestr)
  return dt if dt is not None else dateutil.parser.parse(datestr)

def intersections(sets):
  ret = sets[0]
  for s in sets[1:]:
    ret = ret.intersection(s)
  return ret

def flat_up_tri_mat_idx(i1, i2):
  '''assume that i1 < i2'''
  return i2 * (i2 - 1) // 2 + i1

def to_up_tri_mat_idx(i):
  i1 = int(math.sqrt(2 * i))
  i2 = i - i1 * (i1 - 1) // 2
  return (i2, i1) if i2 < i1 else (i2 - i1, i1 + 1)

def lag_diff(values):
  cnt = len(values)
  for i in range(1, cnt):
    idx = cnt - i
    v = values[idx]
    values[idx] = v - values[idx - 1]
  values[0] = 0.0
  return values

def idx_of_predicate(values, f):
  if values:
    mini = 0
    for i in range(1, len(values)):
      if f(values[i], values[mini]):
        mini = i
    return mini
  return None

def idx_of_min(values):
  return idx_of_predicate(values, lambda x,y: x < y)

def idx_of_max(values):
  return idx_of_predicate(values, lambda x,y: x > y)

def sidx_of_days_ago(days, data):
  cnt, ldate = len(data), data[-1][0].date()
  sdate = ldate - datetime.timedelta(days=days)
  sidx = (0 if days >= cnt else cnt - days)
  if data[sidx][0].date() < sdate:
    sidx = sidx + 1
    while sidx < cnt and data[sidx][0].date() < sdate:
      sidx = sidx + 1
  else:
    while sidx > 0 and data[sidx - 1][0].date() >= sdate:
      sidx = sidx - 1
  return sidx

def create_correlations(cache, days, symbols):
  correlations = [FundCorrelations(cache.base, cache, d) for d in days]
  for c in correlations:
    c.add(symbols)
    c.save()
  return correlations

def align_data(dat1, dat2, lookback_days=None):
  d = {k[0]: [k[0], None, None] for k in dat1}
  d2 = {k[0]: [k[0], None, None] for k in dat2 if k[0] not in d}
  d.update(d2)
  for v in dat1: d[v[0]][1] = v[1]
  for v in dat2: d[v[0]][2] = v[1]
  r = [[k, d[k][1], d[k][2]] for k in sorted(d.keys())]
  sidx = 0
  while sidx < len(r) and ((r[sidx][1] is None) or (r[sidx][2] is None)):
    sidx = sidx + 1
  if sidx > 0:
    r = r[sidx:]
  for i in range(1, len(r)):
    if r[i][1] is None: r[i][1] = r[i - 1][1]
    if r[i][2] is None: r[i][2] = r[i - 1][2]
  if r and lookback_days:
    idx = sidx_of_days_ago(lookback_days, r)
    if idx:
      return r[idx:]
  return r

class FundInfo(object):

  def __init__(self, symbol, name, category):
    self.symbol, self.name, self.category = symbol, name, category

  def __str__(self):
    return f"{self.category} {self.symbol} {self.name}"

class RoundRobinList(object):

  def __init__(self, queue):
    self.queue, self.current = queue, 0

  def next(self):
    item = self.queue[self.current]
    self.current = (self.current + 1) % len(self.queue)
    return item

class FundStat(object):

  def __init__(self, variables):
    '''
      fS_name /*基金或股票信息*/
      fS_code 
      fund_sourceRate /*原费率*/
      fund_Rate /*现费率*/
      fund_minsg /*最小申购金额*/
      stockCodes /*基金持仓股票代码*/
      zqCodes /*基金持仓债券代码*/
      stockCodesNew /*基金持仓股票代码(新市场号)*/
      zqCodesNew /*基金持仓债券代码（新市场号）*/
      syl_1n /*收益率*//*近一年收益率*/
      syl_6y /*近6月收益率*/
      syl_3y /*近三月收益率*/
      syl_1y /*近一月收益率*/
      Data_fundSharesPositions /*股票仓位测算图*/
      Data_netWorthTrend /*单位净值走势 equityReturn-净值回报 unitMoney-每份派送金*/
      Data_ACWorthTrend /*累计净值走势*/
      Data_grandTotal /*累计收益率走势*/
      Data_rateInSimilarType /*同类排名走势*/
      Data_rateInSimilarPersent /*同类排名百分比*/
      Data_fluctuationScale /*规模变动 mom-较上期环比*/
      /*持有人结构*/
      Data_assetAllocation /*资产配置*/
      Data_performanceEvaluation /*业绩评价 ['选股能力', '收益率', '抗风险', '稳定性','择时能力']*/
      Data_currentFundManager /*现任基金经理*/
      Data_buySedemption /*申购赎回*/
      swithSameType /*同类型基金涨幅榜（页面底部通栏）*/
    '''
    for k, v in variables.items():
      setattr(self, k, v)
    self.enabled = "Data_ACWorthTrend" in variables

  def net_worth(self):
    return self.Data_ACWorthTrend[0]

  def real_net_worth(self):
    def convert(v):
      unit_money = v['unitMoney']
      er = v['equityReturn']
      if unit_money:
        return [v['x'], v['y'], unit_money[0], unit_money[1], er]
      return [v['x'], v['y'], None, None, er]
    return [convert(v) for v in self.Data_netWorthTrend[0]]

  def stat_since_date(self, dt=None):
    if dt is not None:
      ldate = self.lastdate()
      if dt >= ldate:
        return None
      return self.stat_last_ndays((ldate - dt).days)
    return self.stat_since_index(0)

  def stat_last_ndays(self, days=None):
    if not self.enabled:
      return None

    if days is not None:
      acworth = self.net_worth()
      if not acworth:
        return None
      return self.stat_since_index(sidx_of_days_ago(days, acworth))
    return self.stat_since_index(0)

  def days_acworth(self, idx=0):
    acworth = self.net_worth()
    cnt = len(acworth)
    if idx > cnt or cnt <= 20:
      return None
    sdate, sworth = acworth[idx]
    sdate = sdate.date()
    x = [(d[0].date() - sdate).days for d in acworth[idx:]]
    # print([d[0].date() for d in acworth[idx:] if d[1] is None])
    y = [d[1] - sworth for d in acworth[idx:]]
    return x, y

  def stat_since_index(self, idx=0):
    x, y = self.days_acworth(idx)
    x = sm.add_constant(x)
    return sm.OLS(y, x).fit()

  def get_acworth(self, days=None):
    if days:
      acworth = self.net_worth()
      sidx = sidx_of_days_ago(days, acworth)
      if sidx:
        sworth = acworth[sidx][1]
        return [[v[0], v[1] - sworth] for v in acworth[sidx:]]
    return self.net_worth()

  def lastdate(self):
    acworth = self.net_worth()
    if not acworth:
      return None
    return acworth[-1][0].date()

class SimpleJSParser(object):

  def __init__(self):
    self.divident = re.compile(r'分红：每份派现金(\d*\.?\d+)元')
    self.split = re.compile(r'拆分：每份基金份额折算(\d*\.?\d+)份')
    self.split1 = re.compile(r'拆分：每份基金份额分拆(\d*\.?\d+)份')

    pass

  def fmt_tm(self, dt):
    return datetime.datetime.fromtimestamp(dt/1000)

  def parse(self, text):
    # Simply split the codes with ';'
    lines = text.split(';')
    variables = {}
    false, null, true = False, None, True # Used in eval
    for l in lines:
      if l:
        comment, definition = l.split("var ")
        varname, varvalue = definition.split("=")
        comment, varname, varvalue = comment.strip(), varname.strip(), eval(varvalue.strip())
        if varname in variables:
          #raise ValueError(f"{varname} redefined")
          print(f"{varname} redefined")
        if varname == "Data_ACWorthTrend":
          varvalue = [(self.fmt_tm(i[0]), i[1]) for i in varvalue if i[1]]
        if varname == "Data_grandTotal":
          for d in varvalue:
            data = [(self.fmt_tm(i[0]), i[1]) for i in d['data'] if i[1]]
            d['data'] = data
            #d.data = [(self.fmt_tm(i[0]), i[1]) for i in d.data]
        if varname == "Data_netWorthTrend":
          cnt = len(varvalue)
          for i in range(0, cnt):
            v = varvalue[i]
            v['x'] = self.fmt_tm(v['x'])
            unit_money = v['unitMoney']

            if unit_money:
              if False:
                if i > 0: print(varvalue[i - 1])
                print(v)
                if i + 1< cnt: print(varvalue[i + 1])
              result = self.divident.match(unit_money)
              if result:
                v['unitMoney'] = ('d', float(result.group(1)))
                continue
              result = self.split.match(unit_money)
              if result:
                v['unitMoney'] = ('s', float(result.group(1)))
                continue
              result = self.split1.match(unit_money)
              if result:
                v['unitMoney'] = ('s', float(result.group(1)))
                continue
              v['unitMoney'] = (v['unitMoney'], None)
              continue
            v['unitMoney'] = None


        variables[varname] = [varvalue, comment]
    # for v, d in variables.items():
    #  print(f"{v} {d[1]}")
    return FundStat(variables)


class FundEastMoneyScraper(object):

  def __init__(self):
    self.funds, self.types = {}, set()
    self.proxies = RoundRobinList([
      #{"http": "socks5://127.0.0.1:9090", "https": "socks5://127.0.0.1:9090",},
      #{"http": "socks5://127.0.0.1:9080", "https": "socks5://127.0.0.1:9080",},
      None,
    ])

  def query(self, url, encoding='utf-8'):
    proxy = self.proxies.next()
    r = requests.get(url, proxies=proxy, timeout=10)
    if r:
      return r.content.decode(encoding).strip() if encoding else r.content
    raise ValueError(f"Failed to query {url}: status code {r.status_code}")

  def daily_info(self, symbol):
    return self.query(f'http://fundgz.1234567.com.cn/js/{symbol}.js')

  def detail_info(self, symbol, encoding='utf-8'):
    content = self.query(f'http://fund.eastmoney.com/pingzhongdata/{symbol}.js', encoding)
    return content

  def extract_funds(self, data):
    content = data.decode('utf-8').strip()[1:] if not isinstance(data, str) else data[1:]
    prefix = 'var r = '
    if not content.startswith(prefix):
      raise ValueError("Response format has changed")
    for f in json.loads(content[len(prefix):].strip(';')):
      self.funds[f[0]] = FundInfo(f[0], f[2], f[3])
    for s, f in self.funds.items():
      self.types.add(f.category)
    return data

  def search(self):
    data = self.query('http://fund.eastmoney.com/js/fundcode_search.js', None)
    return self.extract_funds(data)

  def company(self):
    content = self.query('http://fund.eastmoney.com/js/jjjz_gs.js')
    prefix = "var gs="
    if not content.startswith(prefix):
      raise ValueError("Response format has changed")

class CachedData(object):

  def __init__(self, base):
    self.base, self.downloader, self.search = base, FundEastMoneyScraper(), 'funcode_search'
    cache_file = os.path.join(self.base, f"funcode_search.js")
    data = self.cached_or_download(self.search, True)
    if data and (not self.downloader.funds):
      self.downloader.extract_funds(data)

  @property
  def funds(self):
    return self.downloader.funds

  def cached_or_download(self, path, read=True, only_cache=False):
    cache_file = os.path.join(self.base, f"{path}.js")
    if not os.path.exists(cache_file):
      if only_cache:
        return None
      data = self.downloader.detail_info(path, None) if path != self.search else self.downloader.search()
      print(f"Saving to cache {cache_file}")
      with open(cache_file, "wb") as f:
        f.write(data)
      return data.decode('utf-8').strip()
    print(f"Reading from cache {cache_file}")
    if read:
      with open(cache_file, "rb") as f:
        return f.read().decode('utf-8').strip()
    return None

  def finfo(self, symbol, only_cache=True):
    info = self.cached_or_download(symbol, only_cache=only_cache)
    if info:
      parser = SimpleJSParser()
      return parser.parse(info)
    return None

  @lru_cache(maxsize=1000)
  def finfo_from_cache(self, s, raise_on_none=True, download_if_no_exist=False):
    finfo = CachedData.finfo(self, s, only_cache=download_if_no_exist)
    if not finfo and raise_on_none:
      raise ValueError(f"Can't find fund info for {s}")
    return finfo

  def stat(self, symbol, ndays=720):
    fund = self.finfo_from_cache(symbol, download_if_no_exist)
    return fund.stat_last_ndays(ndays) if fund else None

  def download(self):
    funds = self.funds.values()
    for f in funds:
      try:
        try:
          info = self.cached_or_download(f.symbol, read=False)
        except Timeout:
          print(f"Timeout to scrap data for {f.symbol}, try again")
          info = self.cached_or_download(f.symbol, read=False)
        if info is None:
          continue
      except:
        print(f"Failed to scrap data for {f.symbol}")
      time.sleep(2)

class DailyCachedData(CachedData):

  def __init__(self):

    # today = moment.date('today')
    # real_date = moment.date('friday') if today.weekday > 5 else today
    real_date = parse_date('today 20 hours ago')
    base_dir = f"/tmp/fund/{real_date.strftime('%Y%m%d')}"
    os.makedirs(base_dir, exist_ok=True)
    CachedData.__init__(self, base_dir)

  def finfo(self, symbol):
    return CachedData.finfo(self, symbol, False)

def create_cached_data(base):
  return DailyCachedData() if base is None else CachedData(base)
    

class FundIndicators(object):

  def __init__(self, ndays, model):
    self.ndays, self.model = ndays, model

  @property
  def day_return(self):
    params = self.model.params
    return params[0] / self.ndays + params[1]

  @property
  def tvalue(self):
    return self.model.tvalues[1]

  @property
  def nobs(self):
    return self.model.nobs

  def summary(self):
    ps, ts = self.model.params, self.model.tvalues
    a0, a1, t0, t1 = ["%.3f" % v for v in [self.day_return*100, ps[1]*100, ts[0], ts[1]]]
    return f"({a0}, {a1})/({t0}, {t1}) n{int(self.nobs)}"

class FundCorrelations(object):

  def __init__(self, base, cache, lookback_days):
    self.cache, self.lookback_days = cache, lookback_days
    self.corr_cache_file = f'{base}/corr_d{lookback_days}.pickle'
    self.symbols, self.correlations = [], []
    if os.path.exists(self.corr_cache_file):
      with open(self.corr_cache_file, "rb") as f:
        self.symbols, self.correlations = pickle.load(f)
    self.s_to_idx = {k: v for v, k in enumerate(self.symbols)}
    self.changed = False

  def save(self):
    if self.changed:
      with open(self.corr_cache_file, "wb") as f:
        pickle.dump((self.symbols, self.correlations), f)
        self.changed = False

  def add(self, symbols):

    for s in symbols:
      if s not in self.s_to_idx:
        self.s_to_idx[s] = len(self.symbols)
        i1 = self.cache.finfo_from_cache(s)
        for si in self.symbols:
          i2 = self.cache.finfo_from_cache(si)
          corr = None
          if i1.enabled and i2.enabled:
            d = align_data(i1.net_worth(), i2.net_worth(), self.lookback_days)
            if d:
              corr = pearsonr([v[1] for v in d], [v[2] for v in d])
          self.changed = True
          self.correlations.append(corr)
        self.symbols.append(s)


  def get(self, s1, s2):
    if s1 == s2:
      return 1.0
    if s1 not in self.s_to_idx:
      self.add([s1, s2] if s2 not in self.s_to_idx else [s1])
    elif s2 not in self.s_to_idx:
      self.add([s2])
    i1, i2 = self.s_to_idx[s1], self.s_to_idx[s2]
    if i1 == i2: return 1.0
    idx = flat_up_tri_mat_idx
    return self.correlations[idx(i1, i2) if i1 < i2 else idx(i2, i1)]

class FundStatComputer(object):

  def do_save(self):
    with open(self.report_cache_file, "wb") as f:
      pickle.dump(self.report, f)

  def save(self):
    if self.changed >= self.max_changed:
      self.do_save()
      self.changed = 0

  def save_if_changed(self):
    if self.changed > 0:
      self.do_save()
      self.changed = 0

  def __init__(self, base, lookback_days, show_progress=True, cache=None):
    self.cache, self.lookback_days = cache or CachedData(base), lookback_days
    self.report_cache_file = f'{base}/report_d{lookback_days}.pickle'
    self.report, self.changed, self.max_changed = {}, 0, 300
    if os.path.exists(self.report_cache_file):
      with open(self.report_cache_file, "rb") as f:
        self.report = pickle.load(f)

    count, cnt = 0, len(self.cache.funds)
    for s, f in self.cache.funds.items():
      if s not in self.report:
        try:
          self.report[s] = self.cache.stat(s, lookback_days)
        except Exception as e:
          print(e)
          self.report[s] = None
        self.changed = self.changed + 1
      count = count + 1
      if show_progress:
        stat = self.report[s]
        msg = (f"No summary for {f}" if not stat else f"Summary for {f}\n{stat.summary()}")
        print(f"{count}/{cnt} {msg}")
      self.save()
    self.save_if_changed()

  def indicators(self, s):
    stat = self.report.get(s, None)
    return FundIndicators(self.lookback_days, stat) if stat else None

class FundStatInfo(object):

  def __init__(self, symbols, lookback_days=None, base=None, cache=None, computers=None):
    self.cache = cache or CachedData(base)
    symbols = set(symbols.split(",")) if isinstance(symbols, str) else symbols 
    if computers is None:
      days = [int(d) for d in lookback_days.split(',')] \
        if isinstance(lookback_days, str) else lookback_days
      computers = [(d, FundStatComputer(base, d, False, cache=self.cache)) for d in days]
    self.computers = computers
    for s, f in self.cache.funds.items():
      if s in symbols:
        for d, c in self.computers:
          stat = c.report.get(s, None)
          print(f"No summary for {f}" if not stat else f"Summary for {f}\n{stat.summary()}")

class FundSelector(object):

  def match(self, f, d, reports):
    if f.symbol in reports and f.category in self.tcategory:
      report = reports[f.symbol]
      if report:
        ind = FundIndicators(d, report)
        if ind.day_return >= self.min_day_return and\
          ind.tvalue >= self.min_tvalue and\
          ind.nobs >= self.min_point_ratio * d:
          return ind 
    return None

  def sort_by_day_return(self, ndays, symbols, n=None):
    part = [(k, v[ndays]) for k, v in symbols.items() if ndays in v]
    part = sorted(part, key=lambda v: -v[1].day_return)
    return part[0:n] if n is not None else part

  def select_by_return_rank(self, days, funds, symbols):
    choose_funds = lambda d:set(str(funds[i[0]].symbol)\
      for i in self.sort_by_day_return(d, symbols, self.max_rank))
    selections = [choose_funds(d) for d in days]
    return intersections(selections)

  def __init__(self, base, lookback_days, categories,
    min_day_return=0.001, min_tvalue=20, min_point_ratio=0.6, max_rank=None):
    days = [int(d) for d in lookback_days.split(',')]
    self.cache = CachedData(base)
    categories = '.*' if categories == '*' else categories
    self.categories = {
      'union_fund': '联接基金', 'qdii_index': 'QDII-指数',
      'other_innovation': '其他创新', 'fixed_income': '固定收益',
      'stock': '股票型', 'bond': '债券型', 'money': '货币型',
      'stock_index': '股票指数', 'mixed': '混合型',
      'leverage': '分级杠杆', 'bond_index': '债券指数',
      'stock_fof': '股票-FOF', 'qdii_etf': 'QDII-ETF',
      'etf_listed': 'ETF-场内', 'mixed_fof': '混合-FOF',
      'dk_bond': '定开债券', 'qdii': 'QDII', 'invest': '理财型'
    }
    self.tcategory = set(v for k, v in self.categories.items() if re.match(categories, k))
    self.min_day_return, self.min_tvalue = min_day_return, min_tvalue
    self.min_point_ratio, self.max_rank = min_point_ratio, max_rank
    self.computers = [(d, FundStatComputer(base, d, False, cache=self.cache)) for d in days]
    symbols, funds, ov_min_ret_dict = {}, self.cache.funds, {}
    for s, f in funds.items():
      indicators = [self.match(f, d, c.report) for (d, c) in self.computers]
      if all(indicators):
        symbols[s] = {i.ndays: i for i in indicators}
        ov_min_ret_dict[s] = min(i.day_return for i in indicators)
    self.selected = self.select_by_return_rank(days, funds, symbols)
    self.selected = sorted(list(self.selected), key=lambda s: -ov_min_ret_dict[s])
    self.correlations = create_correlations(self.cache, days, self.selected)
