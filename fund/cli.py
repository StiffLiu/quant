import click
from fund_data_scraper import *

@click.group()
@click.option("--base",
  default="/var/data/raw/fund.eastmoney.com/latest")
@click.pass_context
def main(ctx, base):
  # ensure that ctx.obj exists and is a dict (in case `cli()` is called
  # by means other than the `if` block below)
  ctx.ensure_object(dict)
  ctx.obj['base'] = base

@main.command()
@click.option("--lookback_days", type=int, required=True, default=720)
@click.option("--show_progress", type=bool, required=False, default=True)
@click.pass_context
def compute(ctx, lookback_days, show_progress):
  computer = FundStatComputer(ctx.obj['base'], lookback_days, show_progress)

@main.command()
@click.pass_context
def download(ctx):
  cd = CachedData(ctx.obj['base'])
  cd.download()

@main.command()
@click.pass_context
@click.option("--lookback_days", type=str, required=True, default="720,360,180")
@click.option("--categories", type=str, required=True, default="*")
@click.option("--min_yearly_return", type=float, required=False, default=None)
def analyze(ctx, lookback_days, categories, min_yearly_return):
  args = {}
  if min_yearly_return:
    args["min_day_return"] = min_yearly_return / 365
  fs = FundSelector(ctx.obj['base'], lookback_days, categories, **args)
  fsi = FundStatInfo(fs.selected, cache=fs.cache, computers=fs.computers)

@main.command()
@click.pass_context
@click.option("--lookback_days", type=str, required=True, default="720,360,180")
@click.option("--symbols", type=str, required=True)
def info(ctx, lookback_days, symbols):
  fsi = FundStatInfo(symbols, lookback_days, ctx.obj['base'])

@main.command()
@click.pass_context
def validate(ctx):
  cd = CachedData(ctx.obj['base'])
  for f in cd.funds:
    cd.finfo(f)

@main.command()
@click.pass_context
@click.option("--lookback_days", type=str, required=True, default="720,360,180")
@click.option("--max_corr", type=float, required=True)
def correlations(ctx, lookback_days, max_corr):
  cd = CachedData(ctx.obj['base'])
  days = [int(d) for d in lookback_days.split(',')]
  correlations = create_correlations(cd, days, [])
  for c in correlations:
    syms, corrs = c.symbols, c.correlations
    corrs1 = [v[0] for v in corrs]
    mins = idx_of_min(corrs1)
    maxs = idx_of_max(corrs1)
    allidx = [i for i in range(0, len(corrs)) if i != mins and i != maxs and corrs[i][0] < max_corr]
    allidx = [maxs, mins] + allidx
    print(f"Days {c.lookback_days}")
    for idx in allidx:
      i, j = to_up_tri_mat_idx(idx)
      print(f"({cd.funds[syms[i]]}, {cd.funds[syms[j]]}): {corrs[idx]}")

if __name__ == "__main__":
  main(obj={})
