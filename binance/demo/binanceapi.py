import requests, json, time, asyncio, pathlib, ssl, websockets
from urllib.parse import urlencode
from datetime import datetime

def rest_request(url, proxy=None):
  time.sleep(1)
  print(url)
  response = requests.get(url, verify=False, proxies=proxy)
  print(response.content)
  print(json.dumps(json.loads(response.content.decode('utf-8')), indent=2))

def rest_apis():
  base, btcusdt = "https://fapi.binance.com", {"symbol":"BTCUSDT"}
  btcusdt_1000 = {"symbol":"BTCUSDT", "limit":"1000"}
  btcusdt_5m = {"symbol":"BTCUSDT", "period": "5m"}
  btcusdt_1m_1000 = {"symbol":"BTCUSD", "interval": "15m", "limit":"1000"}
  apis = {"fapi/v1": [
    "exchangeInfo", "ping", "time",
    ("depth", [btcusdt_1000]),
    ("trades", [btcusdt_1000]),
    # historicalTrades
    # aggTrades
    ("klines", [btcusdt_1m_1000]),
    # continuousKlines 
    ("premiumIndex", [btcusdt]),
    "fundingRate",
    ("ticker/24hr", [btcusdt]),
    "ticker/price",
    "ticker/bookTicker",
    # "allForceOrders",
    ("openInterest", [btcusdt]),
    ("lvtKlines", [btcusdt_1m_1000]),
    "indexInfo",
   
  ], "futures/data":[
    ("openInterestHist", [btcusdt_5m]),
    ("topLongShortAccountRatio", [btcusdt_5m]),
    ("topLongShortPositionRatio", [btcusdt_5m]),
    ("globalLongShortAccountRatio", [btcusdt_5m]),
    ("takerlongshortRatio", [btcusdt_5m]),
  ]}
  proxies = {
    "http": "socks5://localhost:3080",
    "https": "socks5://localhost:3080",
  }
  for prefix, subapis in apis.items():
    for api in subapis:
      if isinstance(api, str):
        rest_request(f"{base}/{prefix}/{api}", proxy=proxies)
      elif isinstance(api, tuple):
        api, parameterlists = api
        for p in parameterlists:
            rest_request(f"{base}/{prefix}/{api}?{urlencode(p)}", proxy=proxies)
      else:
        raise TypeError(f"Unsupported type {type(api)}")

async def wss_response(url, ssl_context):
  async with websockets.connect(url, ssl=ssl_context) as ws:
    subscribes = {
      "method": "SUBSCRIBE",
      "params": ["btcusdt@depth@100ms"],
      "id": 1
    }
    await ws.send(json.dumps(subscribes))
    resp = await ws.recv()
    print(resp)
    last_t, pu = None, None
    while True:
      resp = await ws.recv()
      print(resp)
      resp = json.loads(resp)
      d = resp["data"]
      b, a, u = d["b"], d["a"], d["u"]
      if "T" in d and "pu" in d:
        t, npu = int(d["T"]), d["pu"]
        pkt_lose = 'PKT_LOSE' if (pu is not None and npu != pu) else ''
      elif "E" in d and "U" in d:
        t, npu = int(d["E"]), d["U"]
        pkt_lose = 'PKT_LOSE' if (pu is not None and npu != pu+1) else ''
      else:
        print(resp)
        continue

      diff_t = (t - last_t) / 1000.0 if last_t else '-'
      print(f'tradetime={datetime.fromtimestamp(t/1000.0).isoformat()}',
        f'interval={diff_t}', f'nb={len(b)}', f'na={len(a)}',
        f'b={b[0] if b else None}', f'a={a[0] if a else None}', pkt_lose)
      last_t, pu = t, u

def wss_apis():
  path = pathlib.Path(__file__)
  ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
  # We don't verify server certificate
  cert_file = path.with_name('localhost.crt')
  ssl_context.check_hostname = False
  ssl_context.verify_mode = ssl.CERT_NONE
  loop = asyncio.get_event_loop()
  def signal_handler(signal, frame):
    loop.stop()
  url = "wss://fstream.binance.com/stream"
  url = "wss://stream.binance.com:9443/stream"
  url = "wss://54.65.68.210/stream"
  url = "wss://localhost:7444/stream"
  loop.run_until_complete(wss_response(url, ssl_context))

if __name__ == "__main__":
  #rest_apis()
  wss_apis()
