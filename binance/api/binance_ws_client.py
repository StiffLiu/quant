import json, ssl, websockets, aiohttp
import asyncio

class BinanceWSClient(object):

  def __init__(self, url, subscribes, processors={}, default_processor=None):
    self.ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
    self.ssl_context.check_hostname = False
    self.ssl_context.verify_mode = ssl.CERT_NONE
    self.url = url
    self.subscribes = subscribes
    self.default_processor = default_processor or self.print_resp
    self.processors = processors
    self.ws_client = None

  def print_resp(self, data):
    print(data)

  def process(self, resp):
    data = json.loads(resp)
    stream = data.get("stream", None)
    self.processors.get(stream, self.default_processor)(data)

  async def subscribe_sent(self):
    pass  

  async def subscribe(self, req):
      ws = self.ws_client
      await ws.send(json.dumps(req))
      resp = await ws.recv()
      self.process(resp)

  async def client(self):
    async with websockets.connect(self.url, ssl=self.ssl_context) as ws:
      self.ws_client = ws

      await self.subscribe(self.subscribes)
      await self.subscribe_sent()

      while True:
        resp = await self.ws_client.recv()
        self.process(resp)

class BianceFutureClient(BinanceWSClient):

  def __init__(self, subscribes):
    url = "wss://localhost:7444/stream"
    subscribes = {
        "method": "SUBSCRIBE",
        "params": ["btcusdt@depth@100ms"],
        "id": 1
    }
    BinanceWSClient.__init__(self, url, subscribes)

class OptionUnderlying(object):

  def __init__(self):
    self.info, self.mark = None, None

  @property
  def latest(self):
    return {"info": self.info, "mark": self.mark}

class OptionData(object):

  def __init__(self):
    self.mark, self.info, self.underlying, self.ticker = None, None, None, None

  @property
  def latest(self):
    return {
      "mark": self.mark,
      "info": self.info,
      "underlying": (None if self.underlying is None else self.underlying.latest),
      "ticker": self.ticker
    }

class BinanceOptionClient(BinanceWSClient):

  def __init__(self):
    url = "wss://localhost:6444/eoptions/stream"
    subscribes = {
        "method": "SUBSCRIBE",
        "params": ["OPTION_PAIR"],
        "id": 1
    }
    processors = {
      "BTC@markPrice": self.markPrice,
      "BTCUSDT@index": self.index
    }
    self.options = {}
    self.underlyings = {}
    self.rest_url = "http://localhost:8001/eapi/v1"
    BinanceWSClient.__init__(self, url, subscribes, processors)

  def get_option(self, symbol):
      option = self.options.get(symbol, None)
      if option is None:
        option = OptionData()
        self.options[symbol] = option
      return option

  def get_underlying(self, symbol):
      underlying = self.underlyings.get(symbol, None)
      if underlying is None:
        underlying = OptionUnderlying()
        self.underlyings[symbol] = underlying
      return underlying

  async def subscribe_option_data(self):
    base_assets = set()

    streams = []
    async def group_and_sub(stream, processor):
      if stream is not None:
        self.processors[stream] = processor
        streams.append(stream)
      if streams and (len(streams) >= 100 or (stream is None)):
        subscribes = {
            "method": "SUBSCRIBE", "params": streams, "id": 1
        }
        print(f"Subscribed {len(streams)} streams")
        await self.subscribe(subscribes)
        await asyncio.sleep(0.5)
        return []
      return streams
      
    streams = []
    for u, v in self.underlyings.items():
      if v.info:
        streams = await group_and_sub(f"{u}@index", self.index)
        base_assets.add(v.info["baseAsset"])
    for a in base_assets:
      streams = await group_and_sub(f"{a}@markPrice", self.markPrice)

    all_options, new_options = set(), set(o for o in self.options)
    while len(new_options) > 0:
      for o in new_options:
        streams = await group_and_sub(f"{o}@ticker", self.ticker)
        all_options.add(o)
      new_options = [o for o in self.options if o not in all_options]
    await group_and_sub(None, None)

  async def subscribe_sent(self):
    async with aiohttp.ClientSession() as session:
      async with session.get(f'{self.rest_url}/exchangeInfo') as resp:
        text = await resp.text()
        exchInfo = json.loads(text)
        contracts = exchInfo['optionContracts']
        for c in contracts:
          self.get_underlying(c["underlying"]).info = c
        
        symbols = exchInfo['optionSymbols']
        for s in symbols:
          option = self.get_option(s["symbol"])
          option.info, option.underlying = s, self.get_underlying(s["underlying"])

        await self.subscribe_option_data()

  def index(self, data):
    data = data["data"]
    self.get_underlying(data["s"]).mark = data

  def markPrice(self, data):
    for d in data["data"]:
      self.get_option(d["s"]).mark = d

  def ticker(self, data):
    data = data["data"]
    self.get_option(data["s"]).ticker = data

  def get_option_data(self, field, symbol):
    result = []
    if symbol is None:
      for o in self.options.values():
        v = getattr(o, field)
        if v is not None:
          result.append(v)
    else:
      o = self.options.get(symbol, None)
      v = getattr(o, field) if o else None
      if v is not None:
        result.append(v)
    return result
