from .HttpsRestPyBind import HttpsClient, HttpRequestData
import json
import pandas as pd

class BinanceRestClient(object):
    def __init__(self, host, port, addr="127.0.0.1", base_url=""):
        self.client = HttpsClient("/home/lg/apps/config/server.crt", "/home/lg/apps/config/server.key")
        self.data = HttpRequestData()
        self.data.host = host
        self.data.addr = addr
        self.data.port = int(port)
        self.data.service = str(port)
        self.base_url = base_url

    def json(self, url):
        self.data.url = f"{self.base_url}/{url}"
        s = self.client.getContent(self.data).strip()
        return json.loads(s)

    def exchangeInfo(self):
        return self.json("exchangeInfo")

def create_bn_future_client():
    return BinanceRestClient("fapi.binance.com", 7443, base_url="/fapi/v1")

def create_bn_option_client():
    return BinanceRestClient("eapi.binance.com", 6443, base_url="/eapi/v1")

def create_bn_spot_client():
    return BinanceRestClient("api.binance.com", 9442, base_url="/api/v3")

class OptionPair(object):
    
    def __init__(self, exp_date, strike, underlying, put=None, call=None):
        self.exp_date, self.strike, self.underlying = exp_date, strike, underlying
        self.put, self.call = put, call

    def key(self):
        return (self.exp_date, self.strike, self.underlying)

    def set(self, s):
        side = s["side"]
        assert side in ["CALL", "PUT"]
        if side == "CALL":
            if self.call is not None:
                raise ValueError(f"CALL already set for {self.key()}")
            self.call = s
        else:
            if self.put is not None:
                raise ValueError(f"PUT already set for {self.key()}")
            self.put = s

    def is_matched(self):
        return (self.call is not None) and (self.put is not None)

class BNOptionClient(object):
    
    def __init__(self):
        self.client = create_bn_option_client()

    def get_option_pairs(self, as_df=False):
        exchInfo = self.client.exchangeInfo()
        symbols = exchInfo['optionSymbols']
        option_pairs = {}
        for s in symbols:
            key = (int(s["expiryDate"]), float(s["strikePrice"]), s["underlying"])
            if key not in option_pairs:
                option_pairs[key] = OptionPair(*key)
            option_pairs[key].set(s)
        if as_df:
            to_row = lambda op:[op.underlying, op.exp_date, op.strike, op.call["symbol"], op.put["symbol"]]
            data = sorted([to_row(op) for op in option_pairs.values() if op.is_matched()])
            columns = ["underlying", "expiryDate", "strikePrice", "call", "put"]
            df = pd.DataFrame(data, columns=columns)
            return df
        return option_pairs

    def mark_price(self):
        return pd.json_normalize(self.client.json("mark"))

    def underlying_px(self, underlyings):
        underlyings = {u for u in underlyings}
        result = []
        for u in sorted(list(underlyings)):
            ret = self.client.json(f"index?underlying={u}")
            ret["underlying"] = u
            result.append(ret)
        return pd.json_normalize(result)

    def get_all_option_data(self):
        option_symbols = self.get_option_pairs(True)
        mark_px = self.mark_price()
        underlying_px = self.underlying_px(option_symbols['underlying'])
        return option_symbols, mark_px, underlying_px