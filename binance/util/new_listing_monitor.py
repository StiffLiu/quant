import time
import traceback
import os
import requests
from slack import WebClient

def message(**msg):
  # Create a slack client
  slack_web_client = WebClient(token=os.environ.get("SLACK_TOKEN"))

  # Post the onboarding message in Slack
  slack_web_client.chat_postMessage(**msg)

def notify_tdsig(msg_str, dry_run):
  msg = {'type': 'section', 'text': {'type': 'mrkdwn', 'text': msg_str}}
  print(msg)
  if dry_run: return
  for i in range(0, 3):
    try:
      message(channel='#new_binance_currency', blocks=[msg])
      return
    except:
      traceback.print_exc()
      print(f"Retrying[{i}] after 1 seconds...")
      time.sleep(1)

def new_currency_monitor():

  currency_list = None
  while True:
    time.sleep(1)
    try:
      # List all currencies' details
      msg = None
      api_response = requests.get("https://localhost:9442/api/v3/exchangeInfo", verify=False).content
      print(api_response)
      return
      new_currency_list = {c.currency:c for c in api_response}
      if currency_list is None:
        msg = "ALL CURRENCY:" + ",".join(sorted(list(new_currency_list.keys())))
        if len(msg) > 500:
          msg = msg[:500] + "......"
      else: 
        new_currencies = {n:c for n, c in new_currency_list.items() if n not in currency_list}
        if new_currencies:
          print(new_currencies)
          new_currencies = sorted(list(new_currencies.keys()))
          msg = "@ann\n NEW_CURRENCY:" + ",".join(new_currencies)
        else:
          print("No new currency")
      currency_list = new_currency_list
      if msg:
        notify_tdsig(msg, False)
    except Exception as e:
      print("Exception when list_currencies: %s\n" % e)
      time.sleep(3)

if __name__ == "__main__":
  new_currency_monitor()
