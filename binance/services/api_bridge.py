from fastapi import FastAPI, Request
from ..api.binance_rest_client import *


class BinanceRestBridge(FastAPI):

  def __init__(self):
    FastAPI.__init__(self)
    self.spot_client = create_bn_spot_client()
    self.future_client = create_bn_future_client()
    self.option_client= create_bn_option_client()

app = BinanceRestBridge()

def request(client, service, r):
  query = r.url.query
  if query:
    service = service + "?" + query
  return client.json(service)

@app.get(f"{app.spot_client.base_url}/{{service:path}}")
async def request_spot(service, r: Request):
  return request(app.spot_client, str(service), r)

@app.get(f"{app.future_client.base_url}/{{service:path}}")
async def request_spot(service, r: Request):
  return request(app.future_client, str(service), r)

@app.get(f"{app.option_client.base_url}/{{service:path}}")
async def request_option(service, r: Request):
  return request(app.option_client, str(service), r)
