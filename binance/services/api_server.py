from fastapi import FastAPI
from typing import Union
from ..api.binance_ws_client import BinanceOptionClient
import asyncio


class BinanceAPI(FastAPI):

  def __init__(self):
    FastAPI.__init__(self)
    self.option_client = BinanceOptionClient()
    self.clients = [self.option_client]

app = BinanceAPI()

async def my_test():
    print("xxxx")

@app.on_event('startup')
async def app_startup():
    asyncio.create_task(app.option_client.client())

@app.get("/eapi/v1/mark")
async def markPrice(symbol: Union[str, None] = None):
  return app.option_client.get_option_data("mark", symbol)

@app.get("/self/option/info")
async def option_info(symbol: Union[str, None] = None):
  option_client = app.option_client
  return app.option_client.get_option_data("info", symbol)

@app.get("/self/option/latest")
async def option_latest(symbol: Union[str, None] = None):
  option_client = app.option_client
  return app.option_client.get_option_data("latest", symbol)
