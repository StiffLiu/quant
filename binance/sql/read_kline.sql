CREATE OR REPLACE FUNCTION read_klines(kline_interval varchar, symb varchar)
RETURNS TABLE(
	tick_time TIMESTAMP WITH TIME ZONE,
	opening_price DOUBLE PRECISION, closing_price DOUBLE PRECISION,
  highest_price DOUBLE PRECISION, lowest_price DOUBLE PRECISION,
  volume DOUBLE PRECISION
)
AS $$
DECLARE
  -- we have continous data since 2021-06-30
  stime TIMESTAMP := '2021-06-29 23:59:59.999';
BEGIN
	IF cast(kline_interval AS interval) = (interval '1m') THEN
    RETURN QUERY SELECT
		  t.start_time, t.opening_price, t.closing_price,
      t.highest_price, t.lowest_price, t.volume
	  FROM klines_1m t
	  WHERE symbol=symb and t.start_time > stime ORDER BY t.start_time;
	ELSE
    RETURN QUERY SELECT
      time_bucket(cast(kline_interval AS interval), t.start_time) stime, first(t.opening_price, t.start_time),
      last(t.closing_price, t.start_time), max(t.highest_price), min(t.lowest_price), sum(t.volume)
	  FROM klines_1m t
	  WHERE symbol=symb and t.start_time > stime group by stime ORDER BY stime;
	END IF;
END $$
LANGUAGE 'plpgsql';
