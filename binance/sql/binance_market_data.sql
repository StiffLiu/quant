-- CREATE DATABASE binance_market_data;
-- \c binance_market_data;
CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;
DROP TABLE klines_1m;
CREATE TABLE klines_1m(
  start_time      TIMESTAMP WITH TIME ZONE NOT NULL,
  opening_price   DOUBLE PRECISION,
  highest_price   DOUBLE PRECISION,
  lowest_price    DOUBLE PRECISION,
  closing_price   DOUBLE PRECISION,
  volume          DOUBLE PRECISION,
  end_time        TIMESTAMP WITH TIME ZONE NOT NULL,
  value           DOUBLE PRECISION,
  num_trades      INT,
  taker_buy_vol   DOUBLE PRECISION,
  taker_buy_val   DOUBLE PRECISION,
  symbol          VARCHAR,
  primary key(start_time, symbol)
);

SELECT create_hypertable('klines_1m', 'start_time', 'symbol', 100);

ALTER TABLE klines_1m SET (
  timescaledb.compress,
  timescaledb.compress_segmentby = 'symbol'
);

SELECT add_compression_policy('klines_1m', INTERVAL '7 days');
