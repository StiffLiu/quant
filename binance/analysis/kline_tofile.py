import requests, json, time, asyncio, pathlib, ssl, websockets
from urllib.parse import urlencode
from datetime import datetime

def rest_request(url):
  response = requests.get(url)
  return json.loads(response.content.decode('utf-8'))

def save_kline(base, exch_info, kline_path, params, suffix):
  url = f"{base}/{exch_info}"
  symbols = rest_request(url)["symbols"]
  for s in symbols:
    time.sleep(2)
    symbol = s["symbol"]
    url = f"{base}/{kline_path}?symbol={symbol}&{params}"
    with open(f"klines/{symbol}{suffix}.txt", "ab+") as f:
      try:
        response = requests.get(url, verify=False)
        if response:
          result = json.loads(response.content.decode('utf-8'))
          if "code" in result:
              print(f"Error {result}")
          f.write(response.content)
          f.write(b'\n\n')
      except e:
          print(f"Excpetion {str(e)}")

    print(f"Got {len(result)} klines from {url}")

def main():
  params = "interval=1m&limit=1000"
  coin_future = ("https://dapi.binance.com", "dapi/v1/exchangeInfo", "dapi/v1/klines", params, "_COIN_FUT")
  usd_future = ("https://fapi.binance.com", "fapi/v1/exchangeInfo", "fapi/v1/klines", params, "_USD_FUT")
  spot = ("https://api.binance.com", "api/v3/exchangeInfo", "api/v3/klines", params, "_SPOT")
  wait_secs = 60 * 60
  while True:
    print(f"Start downloading data...")
    save_kline(*spot)
    save_kline(*usd_future)
    save_kline(*coin_future)
    print(f"Wait for {wait_secs} seconds...")
    time.sleep(wait_secs)

if __name__ == "__main__":
  main()
