import requests, json, time, asyncio, pathlib, ssl, websockets, psycopg2, psycopg2.extras
from urllib.parse import urlencode
from datetime import datetime

def rest_request(url):
  response = requests.get(url)
  return json.loads(response.content.decode('utf-8'))

def get_conn(dsn, password):
  conn = psycopg2.connect(dsn, password=password)
  columns = [
    "start_time", "opening_price", "highest_price", "lowest_price",
    "closing_price", "volume", "end_time", "value", "num_trades",
    "taker_buy_vol", "taker_buy_val", "symbol"
  ]
  time_cols = {"start_time", "end_time"}
  str_cols = {"symbol"}
  primary_keys = {"start_time", "symbol"}
  def format_str(c):
    if c in time_cols:
      return "to_timestamp(%s/1000.0)"
    # if c in str_cols:
    #  return '"%s"'
    return '%s'

  s_columns = ",".join(columns)
  s_values = ",".join(format_str(c) for c in columns)
  s_updates = ",".join(f"{c}=excluded.{c}" for c in columns)

  sql = f'insert into klines_1m({s_columns}) values ({s_values}) on conflict(start_time, symbol) do update set {s_updates}'
  return conn, sql

def save_kline_with_symbols(dsn, password, symbols, base, kline_path, params, suffix):
  for symbol in symbols:
    url, result = f"{base}/{kline_path}?symbol={symbol}&{params}", None
    try:
      response = requests.get(url, verify=False)
      if response:
        result = json.loads(response.content.decode('utf-8'))
        if "code" in result:
            print(f"Error {result}")
    except Exception as e:
      print(f"Excpetion {str(e)}")
    print(f"{datetime.now()} Got {len(result)} klines from {url}")
    if result:
      s = f"{symbol}{suffix}"
      for r in result:
        r[-1] = s
      for i in range(0, 3):
        try:
          conn, sql = get_conn(dsn, password)
          cursor = conn.cursor()
          psycopg2.extras.execute_batch(cursor, sql, result, page_size=1000)
          #cursor.executemany(sql, result)
          cursor.close()
          conn.commit()
          conn.close()
          break
        except Exception as e:
          print(f"Excpetion {str(e)} retrying {i}")

def save_kline(dsn, password, base, exch_info, kline_path, params, suffix):
  url = f"{base}/{exch_info}"
  symbols = rest_request(url)["symbols"]
  symbols = [s["symbol"] for s in symbols]
  save_kline_with_symbols(dsn, password, symbols, base, kline_path, params, suffix)

def save_option_underlying_klines():
  wait_secs, dsn, password = 5 * 60 * 60, "postgres://postgres@localhost:5432/my_demo", "123456"
  base, kline_path, params, suffix = "http://localhost:8001", "fapi/v1/klines", "interval=1m&limit=1500", "_USD_FUT"
  symbols = ["BTCUSDT", "DOGEUSDT", "ETHUSDT", "BNBUSDT", "SOLUSDT", "XRPUSDT", "AIUSDT"]
  while True:
    save_kline_with_symbols(dsn, password, symbols, base, kline_path, params, suffix)
    time.sleep(wait_secs)

def main():
  params = "interval=1m&limit=1000"
  coin_future = ("https://dapi.binance.com", "dapi/v1/exchangeInfo", "dapi/v1/klines", params, "_COIN_FUT")
  usd_future = ("https://fapi.binance.com", "fapi/v1/exchangeInfo", "fapi/v1/klines", params, "_USD_FUT")
  spot = ("https://api.binance.com", "api/v3/exchangeInfo", "api/v3/klines", params, "_SPOT")
  wait_secs, dsn, password = 9 * 60 * 60, "postgres://postgres@localhost:5433/binance_market_data", "123456"
  while True:
    print(f"Start downloading data...")
    save_kline(dsn, password, *spot)
    save_kline(dsn, password, *usd_future)
    save_kline(dsn, password, *coin_future)
    print(f"Wait for {wait_secs} seconds...")
    time.sleep(wait_secs)

if __name__ == "__main__":
  save_option_underlying_klines()
