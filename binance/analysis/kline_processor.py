import json, glob
import psycopg2
from datetime import datetime

def merge_klines(pathes):
  last, klines = None, []
  for path in pathes:
    with open(path, "r") as f:
      for l in f:
        l = l.strip()
        if l == "":
          continue
        d = json.loads(l)
        if last is None:
          last = d
          continue
        et = d[0][0]
        for v in last:
          if v[0] >= et:
            break
          klines.append(v)
  if last:
    klines = klines + last
  return klines

def to_klines(f):
  result = {}
  for l in f:
    l = l.strip()
    if l:
      d = json.loads(l)
      for v in d:
        result[int(v[0])] = v
  return [result[k] for k in sorted(result.keys())]

def to_klines1(path):
  cutoff_date = (datetime.now().timestamp() - 10 * 24 * 3600) * 1000
  with open(path, "r") as f:
    print(f"Processing file {path}")
    s, result = path.split('/')[-1].split('.')[0], {}
    values = to_klines(f)
    for v in values:
      v[-1] = s
    if values and int(values[-1][0]) < cutoff_date:
      print(f"Ignoring old data in file {path}")
      return []
    return values

def convert_to_csv(pathes, dest):
  klines = merge_klines(pathes)
  with open(dest, "w") as f:
    f.write("t,o,h,l,c,v\n")
    for k in klines:
      k[0] = str(k[0])
      f.write(",".join(k[0:6]) + "\n")

def convert_to_csv1(pathes, dest):
  with open(dest, "w") as wf:
    for path in pathes:
      for v in to_klines1(path):
        v = [str(i) for i in v]
        wf.write(",".join(v) + "\n")

def to_postgresdb(pathes):
  dsn = "postgres://postgres@localhost/binance_market_data"
  conn = psycopg2.connect(dsn, password="123456")
  columns = [
    "start_time", "opening_price", "highest_price", "lowest_price",
    "closing_price", "volume", "end_time", "value", "num_trades",
    "taker_buy_vol", "taker_buy_val", "symbol"
  ]
  time_cols = {"start_time", "end_time"}
  str_cols = {"symbol"}
  primary_keys = {"start_time", "symbol"}
  def format_str(c):
    if c in time_cols:
      return "to_timestamp(%s/1000.0)"
    # if c in str_cols:
    #  return '"%s"'
    return '%s'

  s_columns = ",".join(columns)
  s_values = ",".join(format_str(c) for c in columns)
  s_updates = ",".join(f"{c}=excluded.{c}" for c in columns)

  sql = f'insert into klines_1m({s_columns}) values ({s_values}) on conflict(start_time, symbol) do update set {s_updates}'
  print(sql)
  for path in pathes:
    cursor = conn.cursor()
    cursor.executemany(sql, to_klines1(path))
    cursor.close()
  conn.commit()
  conn.close()

def main():
  files = glob.glob("/home/lg/data/archive/20210413/*.txt")
  to_postgresdb(files)

if __name__ == "__main__":
  main()
