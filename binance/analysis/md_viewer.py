import os, importlib, sys
import kline_analysis as ka
from flask import Flask, request
from flask_compress import Compress

app = Flask(__name__)
Compress(app)

def process(request):
  importlib.reload(sys.modules["kline_analysis"])
  return ka.create_highstock_graph(request)

base_dir = "/home/lg/dev/zen/trunk/src/html/"
@app.route("/", defaults={"path":""})
@app.route("/<path:path>")
def hello_world(path):
  error = "ATTENTION: Your UNAUTHORIZED ACCESS has been recorded!"
  if "../" in path or "/.." in path:
    return error

  if path == "hist_md":
    return process(request)

  fpath = base_dir + path
  if not os.path.isfile(fpath):
    return error
  with open(fpath, "rb") as f:
    return f.read()
