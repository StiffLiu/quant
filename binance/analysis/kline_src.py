import pandas as pd
import traceback, json
from sqlalchemy import create_engine

class PGSrc(object):

  def __init__(self, symbol, table="klines_1m", interval=None):
    self.uds = "postgresql://postgres:123456@localhost/binance_market_data"
    self.engine, self._data, self.symbol = create_engine(self.uds), None, symbol
    self.table, self.interval = table, (interval or "1m")
    self.sql = f"select * from read_klines('{self.interval}', '{self.symbol}')"

  @property
  def data(self):
    return pd.read_sql_query(self.sql, con=self.engine)
