import traceback, json
import numpy as np
import math
from ta import add_all_ta_features
from ta.utils import dropna
from ta.volatility import BollingerBands
from ta.trend import EMAIndicator, MACD
from ta.momentum import RSIIndicator
from .kline_src import *
from my_qlib import OSTreed

def op_cp(df, i):
  row = df.iloc[i]
  return row["opening_price"], row["closing_price"]
  
def lp_hp(df, i):
  row = df.iloc[i]
  return row["lowest_price"], row["highest_price"]

def bar_return(row, f1, f2, fee):
  return row[f1] / row[f2] - 1.0 - fee

def ho_return(row, fee=0.0):
  return bar_return(row, "highest_price", "opening_price", fee)

def co_return(row, fee=0.0):
  return bar_return(row, "closing_price", "opening_price", fee)

def cl_return(row, fee=0.0):
  return bar_return(row, "closing_price", "lowest_price", fee)

def hl_return(row, fee=0.0):
  return bar_return(row, "highest_price", "lowest_price", fee)

def add_runs(df):
  df, last = df.assign(runs=0), 0
  for i in range(0, df.shape[0]):
    op, cp = op_cp(df, i)
    if cp == op: last = 0
    elif cp < op: last = (-1 if last >= 0 else last - 1)
    else: last = (1 if last <= 0 else last + 1)
    df.at[i, "runs"] = last
  return df

def add_volume_percentile(df, window=60*24*4, min_ret=.002, mincnt_ratio=0.1):
  mincnt = int(mincnt_ratio*window)
  tree = OSTreed(mincnt=mincnt)
  df = df.assign(volume_percentile=math.nan)
  for idx, r in df.iterrows():
    v = r["volume"]
    if ho_return(r) >= min_ret:
      tree.push(v)
    if tree.size() > mincnt:
      df.at[idx, "volume_percentile"] = tree.percentile_of_key(v)

    if idx >= window:
      tree.pop()
  return df

def add_return_percentile(df, window=60*24*4, min_ret=.002, mincnt_ratio=0.1):
  mincnt = int(mincnt_ratio*window)
  tree = OSTreed(mincnt=mincnt)
  df = df.assign(return_percentile=math.nan)
  for idx, r in df.iterrows():
    ret = co_return(r)
    if ho_return(r) >= min_ret:
      tree.push(ret)
    if tree.size() > mincnt:
      df.at[idx, "return_percentile"] = tree.percentile_of_key(ret)

    if idx >= window:
      tree.pop()
  return df

def add_volume_ratio(df, window=60*24*4, min_ret=.002, mincnt_ratio=0.1, percentile=0.5):
  mincnt = int(mincnt_ratio*window)
  tree = OSTreed(mincnt=mincnt)
  df = df.assign(volume_ratio=math.nan)
  for idx, r in df.iterrows():
    v = r["volume"]
    if tree.size() > mincnt:
      df.at[idx, "volume_ratio"] = v / tree.find_by_percentile(percentile)

    if ho_return(r) >= min_ret:
      tree.push(v)
    if idx >= window:
      tree.pop()
  return df
  

class BarIndicator(object):

  def __init__(self, symbol, interval=None):
    self.data_src = PGSrc(symbol=symbol, interval=interval)
    self._data = None

  @property
  def data(self):
    if not self._data:
      self._data = self.calc()
    return self._data

  def calc(self):
    # Load datas
    df = self.data_src.data
    # Clean NaN values
    df = dropna(df)
    # Add ta features filling NaN values
    df = add_all_ta_features(df,
      open="opening_price", high="highest_price", low="lowest_price",
      close="closing_price", volume="volume", fillna=True)
    close = df["closing_price"]
    # Initialize Bollinger Bands Indicator
    bb = BollingerBands(close=close, window=100, window_dev=2)
    ema_short = EMAIndicator(close, window=100)
    ema_long = EMAIndicator(close, window=300)
    macd = MACD(close, window_slow=300, window_fast=100, window_sign=75)
    rsi = RSIIndicator(close, window=14)
    # Add Bollinger Bands features
    df["bb_bbm"] = bb.bollinger_mavg()
    df["bb_bbh"] = bb.bollinger_hband()
    df["bb_bbl"] = bb.bollinger_lband()
    # Add Bollinger Band high indicator
    df["bb_bbhi"] = bb.bollinger_hband_indicator()
    # Add Bollinger Band low indicator
    df["bb_bbli"] = bb.bollinger_lband_indicator()
    df["ema_short"] = ema_short.ema_indicator()
    df["ema_long"] = ema_long.ema_indicator()
    df["macd"] = macd.macd()
    df["macd_signal"] = macd.macd_signal()
    df["macd_diff"] = macd.macd_diff()
    df["rsi"] = rsi.rsi()
    return df

class BarStats(object):

  def __init__(self, symbol, interval=None):
    self.data_src = PGSrc(symbol=symbol, interval=interval)
    self.df = self.data_src.data
    self.df = add_runs(self.df)

  def direction_stats(self, num, threshold=200):
    df = self.df
    runs, times, total, cnt = df["runs"], df["tick_time"], 0, 0
    points, probabilities = [], []
    if num > 0:
      for i in range(0, df.shape[0] - 1):
        if runs[i] == num:
          total = total + 1
          if runs[i + 1] < 0:
            cnt = cnt + 1
            if cnt > threshold:
              points.append(times[i])
              probabilities.append(cnt / float(total))
    elif num < 0:
      for i in range(0, df.shape[0]):
        if runs[i] == num:
          total = total + 1
          if runs[i + 1] > 0:
            cnt = cnt + 1
            if cnt > threshold:
              points.append(times[i + 1])
              probabilities.append(cnt / float(total))
    return points, probabilities

  def next_return(self, num, forward=1, is_long=True, with_idx=False):
    df, returns, idx = self.df, [], []
    points, times, runs = [], df["tick_time"], df["runs"]
    for i in range(0, df.shape[0] - forward):
      if runs[i] == num:
        op, cp = op_cp(df, i)
        lp, hp = lp_hp(df, i + 1)
        for j in range(1, forward + 1):
          nlp, nhp = lp_hp(df, i + j)
          if nhp > hp:
            hp = nhp
          if nlp < lp:
            lp = nlp
        points.append(times[i + 1])
        returns.append(hp/cp - 1)
        if with_idx:
          idx.append(i)
    return (points, returns, idx) if with_idx else (points, returns)

def get_ts(row):
  return int(row["tick_time"].timestamp() * 1000)

def get_ohlc(ts, row):
  return [ts, row["opening_price"], row["highest_price"], row["lowest_price"], row["closing_price"]]

def get_volume(ts, row):
  return [ts, row["volume"]]

def create_strat1_graph(args):
  try:
    symbol = args.get("symbol", None)
    if symbol:
      ind = BarIndicator(symbol, interval=args.get("interval", None))
      data = ind.calc()
      data = add_volume_percentile(data, window=12*24*4, min_ret=0)
      ema_long, ema_short, ohlc, volume, bbh, bbl, volume_percentile = [], [], [], [], [], [], []
      macd, macd_diff, macd_signal, rsi, lsig, ssig = [], [], [], [], [], []
      for i, row in data.iterrows():
        ts = get_ts(row)
        ohlc.append(get_ohlc(ts, row))
        volume.append(get_volume(ts, row))

        vp = row["volume_percentile"]
        if not np.isnan(vp):
          volume_percentile.append([ts, vp * 100])

        ema_long.append([ts, row["ema_long"]])
        ema_short.append([ts, row["ema_short"]])
        bbh.append([ts, row["bb_bbh"]])
        bbl.append([ts, row["bb_bbl"]])
        macd.append([ts, row["macd"]])
        macd_signal.append([ts, row["macd_signal"]])
        macd_diff.append([ts, row["macd_diff"]])
        rsi.append([ts, row["rsi"]])

      assert(len(rsi) == len(macd))
      ma_long, ma_short= [], []
      if len(macd):
        for i in range(1, len(macd) - 1):
          if macd[i - 1][1] * macd[i][1] < 0:
            if macd[i][1] > 0:
              ma_long.append([macd[i][0], ohlc[i][4]])
            elif macd[i][1] < 0:
              ma_short.append([macd[i][0], ohlc[i][4]])
          if macd[i][1] < 0 and rsi[i][1] > 70:
            ssig.append([macd[i][0], ohlc[i][4]])
          if macd[i][1] > 0 and rsi[i][1] < 30:
            lsig.append([macd[i][0], ohlc[i][4]])
            

      rsi_over_bought = [v for v in rsi if v[1] > 70]
      rsi_over_sold = [v for v in rsi if v[1] < 30]

      series = [
        {"type": "candlestick", "id": "ohlc", "name": "Stock Price", "data": ohlc},
        {"data": ema_long, "name": "EMA Long"},
        {"data": ema_short, "name": "EMA Short"},
        {"data": bbh, "name": "Bollinger high band"},
        {"data": bbl, "name": "Bollinger low band"},
        {"type": "column", "id": "volume", "name": "Volume", "data": volume, "yAxis": 1},
        #{"type": "column", "id": "macd_diff", "name": "MACD Diff", "data": macd_diff, "yAxis": 2},
        {"name": "MACD Signal", "data": macd_signal, "yAxis": 2},
        #{"type": "scatter", "id": "ma_short", "name": "MA Short", "data": ma_short, "color": "purple"},
        #{"type": "scatter", "id": "ma_long", "name": "MA Long", "data": ma_long, "color": "purple"},
        {"type": "scatter", "id": "ssig", "name": "Short Signal", "data": ssig, "color": "green"},
        {"type": "scatter", "id": "lsig", "name": "Long Signal", "data": lsig, "color": "red"},
        {"name": "MACD", "data": macd, "yAxis": 2},
        {"name": "RSI", "data": rsi, "yAxis": 3},
        {"name": "VolumePercentile", "data": volume_percentile, "yAxis": 3},
        {"type": "scatter", "id": "rsi_over_bought", "name": "RSI Over Bought", "data": rsi_over_bought, "color": "green", "yAxis": 3},
        {"type": "scatter", "id": "rsi_over_sold", "name": "RSI Over Sold", "data": rsi_over_sold, "color": "red", "yAxis": 3},
      ]
      graph = {
        "time": {"timezone": "Asia/Shanghai"},
        "yAxis": [
           {"labels": {"align": "left"}, "height": "60%", "resize": {"enabled": True}},
           {"labels": {"align": "left"}, "top": "60%", "height": "10%", "offset": 0},
           {"labels": {"align": "left"},
             "top": "70%", "height": "10%", "offset": 0,
             "plotLines": [{"color": "#FF0000", "width":1, "value": 0}]},
           {"labels": {"align": "left"},
             "top": "80%", "height": "10%", "offset": 0,
             "plotLines": [{"color": "#FF0000", "width":1, "value": 0}]},
         ],
         "series": series,
         "rangeSelector": False,
      }

      return json.dumps(graph)
  except:
    traceback.print_exc()
  return "{}"

def create_strat2_graph(args):
  try:
    symbol = args.get("symbol", None)
    if symbol:
      ind = BarStats(symbol, interval=args.get("interval", None))
      data, num = ind.df, 4
      data = add_volume_ratio(data, window=12*24*4, min_ret=0)
      data = add_volume_percentile(data, window=12*24*7, min_ret=0)
      data = add_return_percentile(data, window=12*24, min_ret=0)
      ohlc, volume, sig, volume_ratio, bar_return = [], [], [], [], []
      for i, row in data.iterrows():
        ts = get_ts(row)
        ohlc.append(get_ohlc(ts, row))
        volume.append(get_volume(ts, row))
        vp, br = row["volume_percentile"], row["return_percentile"]
        if np.isnan(vp):
          if not np.isnan(br):
            bar_return.append((ts, br))
          continue
        elif np.isnan(br):
          volume_ratio.append([ts, vp])
          continue
        if vp >= 0.98 and (br >= 0.99 or br <= 0.01):
            sig.append([ts, row["closing_price"]])
        volume_ratio.append([ts, vp])
        bar_return.append((ts, br))


      series = [
        {"type": "candlestick", "id": "ohlc", "name": "Stock Price", "data": ohlc},
        {"name": "VolumeRatio", "data": volume_ratio, "yAxis": 1},
        {"name": "BarReturn", "data": bar_return, "yAxis": 1},
        {"type": "column", "id": "volume", "name": "Volume", "data": volume, "yAxis": 2},
        {"type": "scatter", "id": "lsig", "name": "Long Signal", "data": sig, "color": "red"},
      ]
      graph = {
        "time": {"timezone": "Asia/Shanghai"},
        "yAxis": [
           {"labels": {"align": "left"}, "height": "60%", "resize": {"enabled": True}},
           {"labels": {"align": "left"}, "top": "60%", "height": "20%", "offset": 0},
           {"labels": {"align": "left"}, "top": "80%", "height": "20%", "offset": 0},
         ],
         "series": series,
         "rangeSelector": False,
      }
      return json.dumps(graph)
  except:
    traceback.print_exc()
  return "{}"

def create_strat3_graph(args):
  try:
    symbol = args.get("symbol", None)
    if symbol:
      ind = BarIndicator(symbol, interval=args.get("interval", None))
      data = ind.calc()

      min_return = 1.002
      volumes = [r["volume"] for _, r in data.iterrows() if r["highest_price"] / r["opening_price"] >= min_return]
      min_volume = np.percentile(volumes, 80)

      ohlc, volume, sig = [], [], []
      for i, row in data.iterrows():
        if volume and volume[-1][1] >= min_volume:
          sig.append([ts, row["closing_price"]])
        ts = get_ts(row)
        ohlc.append(get_ohlc(ts, row))
        volume.append(get_volume(ts, row))

      series = [
        {"type": "candlestick", "id": "ohlc", "name": "Stock Price", "data": ohlc},
        {"type": "column", "id": "volume", "name": "Volume", "data": volume, "yAxis": 1},
        {"type": "scatter", "id": "lsig", "name": "Long Signal", "data": sig, "color": "red"},
      ]
      graph = {
        "time": {"timezone": "Asia/Shanghai"},
        "yAxis": [
           {"labels": {"align": "left"}, "height": "80%", "resize": {"enabled": True}},
           {"labels": {"align": "left"}, "top": "80%", "height": "20%", "offset": 0},
         ],
         "series": series,
         "rangeSelector": False,
      }
      return json.dumps(graph)
  except:
    traceback.print_exc()
  return "{}"

def create_strat4_graph(args):
  try:
    symbol = args.get("symbol", None)
    if symbol:
      ind = BarStats(symbol, interval=args.get("interval", None))
      data, num, last_row = ind.df, 4, None
      data = add_volume_percentile(data, window=12*24*4, min_ret=0)
      ohlc, volume, osig, csig, max_ret, stop_gain = [], [], [], [], -0.003, None

      for i, row in data.iterrows():
        ts = get_ts(row)
        ohlc.append(get_ohlc(ts, row))
        volume.append(get_volume(ts, row))
        if last_row is not None:
          lvp, last_runs, last_ret = last_row["volume_percentile"], last_row["runs"], co_return(last_row)
          if (not np.isnan(lvp)) and lvp >= 0.98 and last_ret <= max_ret and last_runs != 0:
            osig.append((ts, row["opening_price"]))
            stop_gain = data.iloc[i - abs(last_runs)]["opening_price"]
        if stop_gain is not None and row["highest_price"] >= stop_gain:
            csig.append((ts, stop_gain))
            stop_gain = None
        last_row = row

      series = [
        {"type": "candlestick", "id": "ohlc", "name": "Stock Price", "data": ohlc},
        {"type": "column", "id": "volume", "name": "Volume", "data": volume, "yAxis": 1},
        {"type": "scatter", "id": "osig", "name": "Open Signal", "data": osig, "color": "red"},
        {"type": "scatter", "id": "csig", "name": "Close Signal", "data": csig, "color": "green"},
      ]
      graph = {
        "time": {"timezone": "Asia/Shanghai"},
        "yAxis": [
           {"labels": {"align": "left"}, "height": "80%", "resize": {"enabled": True}},
           {"labels": {"align": "left"}, "top": "80%", "height": "20%", "offset": 0},
         ],
         "series": series,
         "rangeSelector": False,
      }
      return json.dumps(graph)
  except:
    traceback.print_exc()
  return "{}"

def create_highstock_graph(request):
  args = request.args
  funcs = {
    "strat1": create_strat1_graph,
    "strat2": create_strat2_graph,
    "strat3": create_strat3_graph,
    "strat4": create_strat4_graph,
  }
  default_strat = lambda x: "{}"
  strat = args.get("strat", None)
  return funcs.get(strat, default_strat)(args)

def main():
  stat = BarStats('DOGEUSDT_SPOT', "1m")
  points, r, idx = stat.next_return(3, 3, with_idx=True)
  df = stat.df
  ratios = []
  for i in range(0, len(idx)):
    row = df.iloc[idx[i]]
    op, cp = row["lowest_price"], row["highest_price"]
    a = (cp / op - 1)
    ratios.append(r[i] / a if a != 0.0 else 'nan')
  print(ratios)

if __name__ == "__main__":
  main()
