from fund.fund_data_scraper import *
from flask import Flask, render_template, request
from flask_compress import Compress
import binance.kline_analysis as ka
import os, re, html, sys, importlib

class WebApp(Flask):

  def __init__(self):
    Flask.__init__(self, __name__)
    self.cache = {}

  def generate_html(self, path, date, html_args, **args):
    dpath = f"/var/data/raw/fund.eastmoney.com/{date}"
    if os.path.exists(dpath):
      fs = FundSelector(dpath, "180,360,720", **args)
      summaries = []
      for s in fs.selected:
        f = fs.cache.funds[s]
        for d, c in fs.computers:
          stat = c.report.get(s, None)
          summaries.append(f"No summary for {f}" if not stat else f"Summary for {f}\n{stat.summary()}")
      result = "\n".join(summaries)
      result = render_template("info.html", fs=fs, result=result, **html_args, title=f"Till date {date}")
      self.cache[path] = result
      return result
    return None

  def get(self, path):
    if path == "hist_md":
      importlib.reload(sys.modules["kline_analysis"])
      return ka.create_highstock_graph(request)

    if "../" in path or "/.." in path:
      return "ERROR"

    base_dir = "/home/lg/dev/zen/trunk/src/html/"
    fpath = base_dir + path
    if os.path.isfile(fpath):
      with open(fpath, "rb") as f:
        return f.read()

    result = self.cache.get(path, None)
    if not result:
      if re.match('\d\d\d\d\d\d\d\d', path):
        return self.generate_html(path, path, {"day_ret_threshold": 0.005}, categories='*')
      if re.match('b\d\d\d\d\d\d\d\d', path):
        return self.generate_html(path, path[1:], {"day_ret_threshold": 0.0005},
          categories='.*bond.*', min_day_return=0.09/365)
    return result or "ERROR"


app = WebApp()
Compress(app)

@app.route('/', defaults={'path':''})
@app.route('/<path:path>')
def main(path):
  return app.get(path) or "NOT FOUND" 
